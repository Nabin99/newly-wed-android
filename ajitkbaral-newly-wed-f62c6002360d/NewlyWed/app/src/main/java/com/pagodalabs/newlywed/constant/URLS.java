package com.pagodalabs.newlywed.constant;

/**
 * Created by Ajit Kumar Baral on 8/3/2015.
 */
public class URLS {
    public static final String pagodalabsUrl = "http://www.newlywed.org.np/",
            newlywedUrl = pagodalabsUrl,
            uploadUrl = newlywedUrl + "uploads/",
            inspirationImageUrl = uploadUrl + "inspiration/thumb/",
            tipImageUrl = uploadUrl + "page/",
            productImageUrl = uploadUrl + "product/",
            jsonInspiration = newlywedUrl + "inspiration/api/inspiration.json",
            jsonProduct = newlywedUrl + "product/api/product",
            jsonProductDetail = newlywedUrl + "product/api/detail?id=",
            jsonTip = newlywedUrl + "tips/api/tips.json",
            jsonSexploration = newlywedUrl + "marital/api/marital.json",
            jsonMarital = newlywedUrl + "marital/api/marital?category_id=",
            maritalImageUrl = uploadUrl + "page/thumb/",
            registerPost = newlywedUrl + "auth/api/register.json",
            loginPost = newlywedUrl + "auth/api/login.json",
            logout = newlywedUrl + "auth/api/logout.json",
            wishlistPost = newlywedUrl + "account/api/wishlist.json",
            wishListUrl = newlywedUrl + "/account/api/wishlist.json?userId=",
            vendorImageUrl = pagodalabsUrl + "/uploads/vendors/",
            categoryImageUrl = uploadUrl + "category/",
            jsonVendorAppointment = newlywedUrl+"vendor/api/vendor.json",
            jsonVendor = newlywedUrl + "vendor/api/vendor?type=",
            jsonVendorType = newlywedUrl + "vendor_type/api/vendor_type.json",
            vendorTypeImageUrl = newlywedUrl + "uploads/vendor_type/",
            jsonProductWithCategory = newlywedUrl + "/product/api/product?cat_name=",
            jsonCalendar = newlywedUrl + "account/api/calendar?userId=",
            jsonCheckList = newlywedUrl + "account/api/checklist?userId=",
            saveCheckList = newlywedUrl + "account/api/checklist.json",
            jsonAppointments = newlywedUrl + "auth/api/appointments.json",
            addAppointment = newlywedUrl+"account/api/appointment.json";
}
