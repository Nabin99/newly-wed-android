package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.SexplorationViewHolder;

import java.util.ArrayList;

import static com.pagodalabs.newlywed.R.drawable.login_background;

/**
 * Created by Ajit Kumar Baral on 12/23/2015.
 */
public class SexplorationAdapter extends RecyclerView.Adapter<SexplorationViewHolder> {
    private ArrayList<Sexploration> sexplorationArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private Context context;
    private String[] ImageName = {"Pregnancy","Sexual_and_Reproductive_health","contraceptives","Safe_Abortion","Councelling_and_Services"};
    private int[] imageName = {R.drawable.family_planning,R.drawable.pregnancy,R.drawable.sexual_and_reproductive_health,R.drawable.contraceptives,R.drawable.safe_abortion,R.drawable.councelling_and_services};
    //private ImageView cardBackground;

    public SexplorationAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

    }

    public void setSexplorationList(ArrayList<Sexploration> sexplorationArrayList){
        if (sexplorationArrayList != null) {
            this.sexplorationArrayList = sexplorationArrayList;
            notifyItemRangeChanged(0, sexplorationArrayList.size());
        }
    }
    @Override
    public SexplorationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_sexploration, parent, false);
        SexplorationViewHolder viewHolderSexploration = new SexplorationViewHolder(view);
        return viewHolderSexploration;
    }

    @Override
    public void onBindViewHolder(final SexplorationViewHolder holder, int position) {
        final Sexploration sexploration = sexplorationArrayList.get(position);
        holder.tvName.setText(sexploration.getName());
        holder.cardBackground.setBackgroundResource(imageName[position]);
        /*if(sexploration.getSubSexplorations() == 0){*/
           // holder.tvSubCategories.setVisibility(View.GONE);

           // holder.cardView.setBackground(R.drawable.family_planning);

        /*}else{
            holder.tvSubCategories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SubCategoriesActivity.class);
                    intent.putExtra("subCategories", sexploration);
                    context.startActivity(intent);
                }
            });
        }*/

    }

    @Override
    public int getItemCount() {
        return sexplorationArrayList.size();
    }
}
