package com.pagodalabs.newlywed.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class VendorDatabase extends SQLiteOpenHelper
{
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "NewlyWed";

    // Contacts table name
    private static final String TABLE_VENDOR_TYPE = "vendorType";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "vendor_name";
    private String id;
    private String vendorName;

    public VendorDatabase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_VENDORTYPE_TABLE = "CREATE TABLE " + TABLE_VENDOR_TYPE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";
        db.execSQL(CREATE_VENDORTYPE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_VENDOR_TYPE);
        onCreate(db);
    }

    // Adding new contact
    public void addContact(VendorType vendortype) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID,vendortype.getVendorTypeId());
        values.put(KEY_NAME,vendortype.getVendorTypeName());

        db.insert(TABLE_VENDOR_TYPE, null, values);
        db.close();

    }

    // Getting All Contacts
    public List<VendorType> getAllContacts() {
        List<VendorType> contactList = new ArrayList<VendorType>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VENDOR_TYPE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                VendorType vendorType = new VendorType();
                vendorType.setVendorTypeId(Integer.parseInt(cursor.getString(0)));
                vendorType.setVendorTypeName(cursor.getString(1));
                // Adding contact to list
                contactList.add(vendorType);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
}
