package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class Marital implements Serializable {

        private int pageId;
        private String pageTitle;
        private String description;
        private String imageName;
        private String modifiedDate;
        private String type;
        private int parent_id;

        public Marital() {
        }

        public Marital(int parent_id,int pageId, String pageTitle, String description, String imageName, String modifiedDate, String type) {
            this.parent_id = parent_id;
            this.pageId = pageId;
            this.pageTitle = pageTitle;
            this.description = description;
            this.imageName = imageName;
            this.modifiedDate = modifiedDate;
            this.type = type;
        }

        public int getPageId() {
            return pageId;
        }

        public void setPageId(int pageId) {
            this.pageId = pageId;
        }

        public String getPageTitle() {
            return pageTitle;
        }

        public void setPageTitle(String pageTitle) {
            this.pageTitle = pageTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    @Override
        public String toString() {
            return "Marital{" +
                    "pageId=" + pageId +
                    ", pageTitle='" + pageTitle + '\'' +
                    ", description='" + description + '\'' +
                    ", imageName='" + imageName + '\'' +
                    ", modifiedDate='" + modifiedDate + '\'' +
                    '}';
        }


}
