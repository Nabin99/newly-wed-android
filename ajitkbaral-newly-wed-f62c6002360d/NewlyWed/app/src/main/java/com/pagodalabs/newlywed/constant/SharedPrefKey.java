package com.pagodalabs.newlywed.constant;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class SharedPrefKey {

    public static final String fileName = "newlyWed", inspiration = "inspiration", product = "product", tip = "tip", marital = "marital";
    public static final String category="category", login="login";
}
