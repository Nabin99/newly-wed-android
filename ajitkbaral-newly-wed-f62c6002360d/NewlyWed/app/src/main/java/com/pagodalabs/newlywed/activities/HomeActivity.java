package com.pagodalabs.newlywed.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;

import static com.pagodalabs.newlywed.R.color.primaryColor;

public class HomeActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private Button btnCalendar;
    private Button btnCheckList;
    private Button btnBlog;
    private Button btnVendors;
    private Button btnSexploration;
    private Button btnLogin;
    private Button btnRegister;
    private BroadcastReceiver broadcastReceiver;
    private WebView homebarText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Intent i = new Intent(this, RegistrationIntentService.class);
        startService(i);

        context = this;
        toolbar = (Toolbar) findViewById(R.id.home_app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        homebarText = (WebView) findViewById(R.id.home_appbar_text);
        homebarText.loadData("<div align = center style=font-family: Myriad pro><b>Welcome to Newly Wed</b></br> From Planning your wedding,</br> to planning your family,</br> we've got you covered.</div>", "text/html", "UTF-8");
        homebarText.setBackgroundColor(Color.TRANSPARENT);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        btnCalendar = (Button)findViewById(R.id.btnCalendar);
        btnCheckList = (Button)findViewById(R.id.btnCheckList);
        btnBlog = (Button)findViewById(R.id.btnBlog);
        btnVendors = (Button)findViewById(R.id.btnVendors);
        btnSexploration = (Button)findViewById(R.id.btnSexploration);

        if(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("")){
            btnCalendar.setVisibility(View.GONE);
            btnCheckList.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
            btnRegister.setVisibility(View.VISIBLE);
        }else if(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("loggedIn")){
            btnCalendar.setVisibility(View.VISIBLE);
            btnCheckList.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);
            btnRegister.setVisibility(View.GONE);
        }

//        btnCalendar.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Utils.buttonTouch(btnCalendar, event);
//                return false;
//            }
//        });
//        btnBlog.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Utils.buttonTouch(btnBlog, event);
//                return false;
//            }
//        });
//        btnVendors.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Utils.buttonTouch(btnVendors, event);
//                return false;
//            }
//        });
//        btnSexploration.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Utils.buttonTouch(btnSexploration, event);
//                return false;
//            }
//        });
//        btnCheckList.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Utils.buttonTouch(btnCheckList, event);
//                return false;
//            }
//        });

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CustomCalendarActivity.class));
            }
        });
        btnBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, InspirationalActivity.class));
            }
        });
        btnSexploration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SexplorationActivity.class));
            }
        });
        btnVendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, VendorTypeActivity.class));
            }
        });
        btnCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CheckListActivity.class));
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, LoginActivity.class));

            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, RegisterActivity.class));
            }
        });

        KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

        /*IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Logout in progress");
                //At this point we should start the login activity and finish this one.
                finish();
            }
        }, intentFilter);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(!SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("")){

            getMenuInflater().inflate(R.menu.default_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }


}
