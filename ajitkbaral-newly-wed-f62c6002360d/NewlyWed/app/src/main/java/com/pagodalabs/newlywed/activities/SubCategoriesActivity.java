package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.SexplorationAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubCategoriesActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvSubCategories;
    private Sexploration sexploration;

    private Context context;

    private SexplorationAdapter sexplorationAdapter;

    private ArrayList<Sexploration> sexplorationArrayList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categories);
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvSubCategories = (RecyclerView) findViewById(R.id.rvSubCategories);
        tvVolleyError = (TextView) findViewById(R.id.tvVolleyError);
        rvSubCategories.setLayoutManager(new LinearLayoutManager(context));
        sexplorationAdapter = new SexplorationAdapter(context);
        rvSubCategories.setAdapter(sexplorationAdapter);

         sexploration = (Sexploration) getIntent().getSerializableExtra("subCategories");

        sendJsonRequest();

        rvSubCategories.addOnItemTouchListener(new RecyclerTouchListener(this, rvSubCategories, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Sexploration sexploration = sexplorationArrayList.get(position);
                Intent maritalDetail = new Intent(context, MaritalActivity.class);
                maritalDetail.putExtra("sexploration", sexploration);
                startActivity(maritalDetail);

            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));


        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonSexploration+"?category_id="+sexploration.getId(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        tvVolleyError.setVisibility(View.GONE);
                        parseJSONResponse(response);
                        sexplorationAdapter.setSexplorationList(sexplorationArrayList);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Sexploration> parseJSONResponse(JSONObject response) {

        sexplorationArrayList = new ArrayList<Sexploration>();
        if (response != null || response.length() > 0) {
            try {

                if (response.has("categories")) {
                    JSONArray jsonArray = response.getJSONArray("categories");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject sexplorationObject = jsonArray.getJSONObject(i);
                        int id = sexplorationObject.getInt("id");
                        String name = sexplorationObject.getString("name");
                        int parentId = sexplorationObject.getInt("parent_id");
                        Sexploration sexploration = new Sexploration(id, name, parentId, 0);
                        sexploration.toString();
                        sexplorationArrayList.add(sexploration);


                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return sexplorationArrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
        Log.d("Newly Wed", "onBackPressed");
    }

}
