package com.pagodalabs.newlywed.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;

/**
 * Created by Ajit Kumar Baral on 12/23/2015.
 */
public class SexplorationViewHolder extends RecyclerView.ViewHolder {
    public TextView tvName;
    public TextView tvSubCategories;
    public CardView cardView;
    public ImageView cardBackground;
    public RelativeLayout cardViewRelativeLayout;

    public SexplorationViewHolder(View itemView) {
        super(itemView);

        tvName = (TextView)itemView.findViewById(R.id.tvName);
        //tvSubCategories = (TextView)itemView.findViewById(R.id.tvSubCategories);
        cardView = (CardView) itemView.findViewById(R.id.cvTip);
        //cardViewRelativeLayout =(RelativeLayout) itemView.findViewById(R.id.cardViewRelativeBackground);
        //cardBackground = (ImageView) itemView.findViewById(R.id.cardImageBackground);
        cardBackground = (ImageView) itemView.findViewById(R.id.cardImage);

    }
}
