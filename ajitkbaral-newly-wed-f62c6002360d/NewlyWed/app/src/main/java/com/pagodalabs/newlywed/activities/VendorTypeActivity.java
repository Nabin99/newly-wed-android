package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.VendorTypeAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.database.DatabaseHandler;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.entities.VendorDatabase;
import com.pagodalabs.newlywed.entities.VendorType;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VendorTypeActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvVendorType;

    private Context context;

    private VendorTypeAdapter vendorTypeAdapter;

    private ArrayList<VendorType> vendorTypeArrayList;
    private ProgressDialog progressDialog;

    private SwipeRefreshLayout swipeRefreshLayout;
    private BroadcastReceiver broadcastReceiver;

    private DatabaseHandler helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_type);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        helper = new DatabaseHandler(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        rvVendorType = (RecyclerView) findViewById(R.id.rvVendorType);
        tvVolleyError = (TextView) findViewById(R.id.tvVolleyError);
        //rvVendorType.setLayoutManager(new GridLayoutManager(context, 3));
        rvVendorType.setLayoutManager(new LinearLayoutManager(context));
        vendorTypeAdapter = new VendorTypeAdapter(context);
        rvVendorType.setAdapter(vendorTypeAdapter);

        if(getDataFromDB() != 0){
            vendorTypeAdapter.setVendorTypeArrayList(vendorTypeArrayList,true);
        } else {
            sendJsonRequest();
        }


            rvVendorType.addOnItemTouchListener(new RecyclerTouchListener(this, rvVendorType, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View v, int position) {
                    VendorType vendorType = vendorTypeArrayList.get(position);

                    Log.d("VendorType:", "" + vendorType.getVendorTypeName());
                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "vendorTypeName", vendorType.getVendorTypeName());
                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "vendorTypeId", String.valueOf(vendorType.getVendorTypeId()));


                    Intent intentVendorActivity = new Intent(context, VendorActivity.class);
                    startActivity(intentVendorActivity);

                }

                @Override
                public void onLongClick(View v, int position) {

                }

            }));

            broadcastReceiver = KillActivity.killActivity(this);
            KillActivity.registerReceiver(context);


        }


    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonVendorType,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        tvVolleyError.setVisibility(View.GONE);
                        helper.clearVendorType();
                        helper = new DatabaseHandler(VendorTypeActivity.this);
                        parseJSONResponse(response);

                        //VendorDatabase.addContact(vendorTypeArrayList);

                        vendorTypeAdapter.setVendorTypeArrayList(vendorTypeArrayList,false);
                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(context, "Latest data updated", Toast.LENGTH_SHORT).show();
                        } else {
                            progressDialog.dismiss();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (vendorTypeArrayList== null || vendorTypeArrayList.size() == 0) {

                            VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);

                        }

                        if (swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                            tvVolleyError.setVisibility(View.GONE);
                            Toast.makeText(context, "Unable to get updates", Toast.LENGTH_SHORT).show();
                        } else {
                            progressDialog.dismiss();
                        }

                    }
                });
        requestQueue.add(request);

        if(!swipeRefreshLayout.isRefreshing()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Fetching Content....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    private ArrayList<VendorType> parseJSONResponse(JSONObject response) {

        vendorTypeArrayList = new ArrayList<VendorType>();
        if (response != null || response.length() > 0) {
            try {

                if (response.has("vendor_types")) {
                    JSONArray arrayVendorTypes = response.getJSONArray("vendor_types");

                    for (int i = 0; i < arrayVendorTypes.length(); i++) {
                        JSONObject vendorTypeJson = arrayVendorTypes.getJSONObject(i);
                        VendorType vendorType = new VendorType();
                        vendorType.setVendorTypeId((vendorTypeJson.getInt("id")));
                        vendorType.setVendorTypeName(vendorTypeJson.getString("type"));
                        vendorType.setVendorTypeImage(vendorTypeJson.getString("image"));
                        helper.saveVendorType(vendorTypeJson.getInt("id"),vendorTypeJson.getString("type"),vendorTypeJson.getString("image"));
                        vendorTypeArrayList.add(vendorType);
                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return vendorTypeArrayList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(!SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("")){

            getMenuInflater().inflate(R.menu.default_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
        Log.d("Newly Wed", "onBackPressed");
    }


    @Override
    public void onRefresh() {
        sendJsonRequest();
    }

    public int getDataFromDB(){
        vendorTypeArrayList = helper.getVendorType();
        return vendorTypeArrayList.size();
    }
}
