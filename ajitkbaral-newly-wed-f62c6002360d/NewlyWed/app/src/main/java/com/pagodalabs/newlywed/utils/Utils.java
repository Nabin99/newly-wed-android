package com.pagodalabs.newlywed.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.activities.AboutUsActivity;
import com.pagodalabs.newlywed.activities.HelperActivity;
import com.pagodalabs.newlywed.activities.HomeActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.singleton.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class Utils {

    private static VolleySingleton volleySingleton=VolleySingleton.getInstance();;
    private static RequestQueue requestQueue=volleySingleton.getRequestQueue();


    public static void buttonTouch(Button btn, MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            btn.setTextColor(Color.CYAN);
        if (event.getAction() == MotionEvent.ACTION_UP)
            btn.setTextColor(Color.WHITE);
    }

    public static String priceInFormat(String price){
        String priceInCommas="";
        NumberFormat anotherFormat = NumberFormat.getNumberInstance(Locale.US);
        if (anotherFormat instanceof DecimalFormat) {
            DecimalFormat anotherDFormat = (DecimalFormat) anotherFormat;
            anotherDFormat.applyPattern("#.00");
            anotherDFormat.setGroupingUsed(true);
            anotherDFormat.setGroupingSize(3);

            priceInCommas = anotherDFormat.format(Double.parseDouble(price));
        }
        return priceInCommas;

    }


    public static void popUpMenu(Context context, int id){
        switch (id){
            case R.id.action_home:
                context.startActivity(new Intent(context, HomeActivity.class));
                break;
            /*case R.id.action_cart:
                context.startActivity(new Intent(context, CartActivity.class));
                break;
            case R.id.action_wish_list:
                context.startActivity(new Intent(context, WishListActivity.class));
                break;*/
            case R.id.action_aboutUs:
                context.startActivity(new Intent(context, AboutUsActivity.class));
                break;
            case R.id.action_logout:
                Intent stopIntent = new Intent("CLOSE_ALL");
               // context.sendBroadcast(stopIntent);

                /*Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                context.sendBroadcast(broadcastIntent);*/
                logout(context, SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "userId", ""));
                break;

        }
    }

    private static void logout(final Context context, String userId) {


        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "userId", "");
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "username", "");
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "accessToken", "");
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "sessionExpire", "");
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "login", "");
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
        ((HomeActivity) context).finish();
       /* HashMap<String, String> params = new HashMap<String, String>();
        params.put("userId", userId);
        Log.d("id","" + userId);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                URLS.logout, new JSONObject(params),
                new Response.Listener<JSONObject>() {


                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response", response.toString());
                        Boolean success;
                        try {
                            if (response.has(Key.success)) {
                                success = (Boolean) response.get(Key.success);
                                if(success){


                                    Intent intent = new Intent(context, HelperActivity.class);
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "userId", "");
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "username", "");
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "accessToken", "");
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "sessionExpire", "");
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "login", "");

                                    context.startActivity(intent);

                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Unable to logout. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                });
        requestQueue.add(request);
        */
    }

    public static String html2text(String html) {

        return html;
    }

}
