package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.ProductAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.database.ProductDatabaseAdapter;
import com.pagodalabs.newlywed.entities.Product;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.Utils;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvEmpty;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvCart;

    private Context context;

    private ProductAdapter productAdapter;

    private ArrayList<Product> productArrayList;
    private ProgressDialog progressDialog;

    private BroadcastReceiver broadcastReceiver;

    private ProductDatabaseAdapter productDatabaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        context = this;

        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        productDatabaseAdapter = new ProductDatabaseAdapter(context);

        rvCart = (RecyclerView) findViewById(R.id.rvCart);
        tvEmpty = (TextView)findViewById(R.id.tvEmpty);
        rvCart.setLayoutManager(new LinearLayoutManager(context));
        productAdapter = new ProductAdapter(context);
        rvCart.setAdapter(productAdapter);

        productArrayList = (ArrayList)productDatabaseAdapter.getAll(Key.categoryCart);
        productAdapter.setProductList(productArrayList);

        if(productArrayList == null || productArrayList.size()<=0){
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText("Cart is Empty");
        }

        rvCart.addOnItemTouchListener(new RecyclerTouchListener(this, rvCart, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                final Product product = productArrayList.get(position);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Remove " + product.getProductName() +" from cart?");
                alertDialog.setMessage("Do yo want to remove "+product.getProductName()+" from cart?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        productDatabaseAdapter.delete(product.getId(), Key.categoryCart);
                        Intent cartIntent = new Intent(context, CartActivity.class);
                        cartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(cartIntent);
                        finish();
                    }
                });
                alertDialog.setNegativeButton("No", null);
                alertDialog.show();
            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));

        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
    }

}
