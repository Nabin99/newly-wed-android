package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.ProductAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Product;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WishListActivity extends AppCompatActivity{

    private Context context;
    private Toolbar toolbar;
    private RecyclerView rvWishList;
    private TextView tvEmpty;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    private ProductAdapter productAdapter;
    private ArrayList<Product> productArrayList;

    private ProgressDialog progressDialog;

    final String results =null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        context = this;

        toolbar = (Toolbar)findViewById(R.id.app_bar);

        setSupportActionBar(toolbar);

        rvWishList = (RecyclerView)findViewById(R.id.rvWishList);
        tvEmpty = (TextView)findViewById(R.id.tvEmpty);
        rvWishList.setLayoutManager(new LinearLayoutManager(context));

        productAdapter = new ProductAdapter(context);
        rvWishList.setAdapter(productAdapter);

        volleySingleton = VolleySingleton.getInstance();

        requestQueue = volleySingleton.getRequestQueue();

        long currentDateInEpoch = System.currentTimeMillis()/1000;
        String expire = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "sessionExpire", "");
        long sessionExpire = 0;
        if(!expire.equals("")){

             sessionExpire = Long.parseLong(expire);
        }

        if(sessionExpire !=0 && currentDateInEpoch<sessionExpire){

            Log.d("Newly Wed", ""+currentDateInEpoch + " "+sessionExpire);

            String userId = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "userId", "");
            String accessToken = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "accessToken", "");

            String url = "http://pagodalabs.com/newlywed/account/api/wishlist?userId=" + userId + "&access_token=" + accessToken+".json";
            Log.d("Newly Wed", url);

            sendJsonRequest(url);

        }else{

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage("Session Expired");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "userId", "");
                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "username", "");
                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName, "sessionExpire", "");
                    startActivity(new Intent(context, HelperActivity.class));
                }
            });
            alertDialog.show();

        }



        rvWishList.addOnItemTouchListener(new RecyclerTouchListener(context, rvWishList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final Product product = productArrayList.get(position);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Remove " + product.getProductName() +" from wish list?");
                alertDialog.setMessage("Do yo want to remove "+product.getProductName()+" from wish list?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent wishListIntent = new Intent(context, WishListActivity.class);
                        wishListIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(wishListIntent);
                        finish();
                    }
                });
                alertDialog.setNegativeButton("No", null);
                alertDialog.show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);
    }



    private void sendJsonRequest(String url) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("Newly Wed", response.toString().replace("[],", ""));
                        tvEmpty.setVisibility(View.GONE);

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response.toString().replace("[],", ""));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        parseJSONResponse(jsonObject);
                        productAdapter.setProductList(productArrayList);
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvEmpty, error);
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<Product> parseJSONResponse(JSONObject response) {

        productArrayList = new ArrayList<Product>();
        if (response != null || response.length() > 0){
            try {

                if (response.has("wishlist")) {
                    JSONArray jsonArray = response.getJSONArray("wishlist");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject productsObject = jsonArray.getJSONObject(i);
                        int id = productsObject.getInt(Key.productId);
                        String productName = productsObject.getString(Key.name);
                        String description = productsObject.getString(Key.productDescription);
                        int quantity = productsObject.getInt(Key.productQuantity);
                        String salePrice = productsObject.getString(Key.productSalePrice);
                        String imageUrl = productsObject.getString(Key.productImageUrl);

                        Log.d("Newly Wed", ""+id+productName+description+quantity+salePrice+imageUrl);


                        productArrayList.add(new Product(id, productName, description, quantity, salePrice, URLS.productImageUrl + id + "/" + imageUrl));

                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return productArrayList;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }
}
