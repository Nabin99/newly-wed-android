package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.SexplorationAdapter;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.database.DatabaseHandler;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.touchlistener.RecyclerTouchListener;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SexplorationActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    private Toolbar toolbar;
    private TextView tvVolleyError;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private RecyclerView rvSexploration;

    private Context context;

    private SexplorationAdapter sexplorationAdapter;

    private ArrayList<Sexploration> sexplorationArrayList;
    private ProgressDialog progressDialog;
    private DatabaseHandler helper;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean refreshStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sexploration);
        context = this;
        helper = new DatabaseHandler(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();


        rvSexploration = (RecyclerView) findViewById(R.id.rvSexploration);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        tvVolleyError = (TextView) findViewById(R.id.tvVolleyError);
        rvSexploration.setLayoutManager(new LinearLayoutManager(context));
        sexplorationAdapter = new SexplorationAdapter(context);
        rvSexploration.setAdapter(sexplorationAdapter);

        if(getDatafromDB() > 0){
            sexplorationAdapter.setSexplorationList(sexplorationArrayList);
        } else {
            refreshStatus = false;
            sendJsonRequest();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshStatus = true;
                sendJsonRequest();
            }
        });

        rvSexploration.addOnItemTouchListener(new RecyclerTouchListener(this, rvSexploration, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Sexploration sexploration = sexplorationArrayList.get(position);
                Intent maritalDetail = new Intent(context, MaritalActivity.class);
                maritalDetail.putExtra("sexploration", sexploration);
                Log.d("jsonRES1", "" + sexploration);
                startActivity(maritalDetail);

            }

            @Override
            public void onLongClick(View v, int position) {

            }

        }));


        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonSexploration,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("jsonRES", "" + response);
                        tvVolleyError.setVisibility(View.GONE);
                        helper.clearSexploration();
                        helper = new DatabaseHandler(SexplorationActivity.this);
                        parseJSONResponse(response);
                        sexplorationAdapter.setSexplorationList(sexplorationArrayList);

                        if(swipeRefreshLayout.isRefreshing()){
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(context, "Latest data updated", Toast.LENGTH_SHORT).show();
                        } else {
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyErrorMessage.handleVolleyErrors(tvVolleyError, error);
                        if(swipeRefreshLayout.isRefreshing()){
                            swipeRefreshLayout.setRefreshing(false);
                            tvVolleyError.setVisibility(View.GONE);
                            Toast.makeText(context, "Unable to get updates", Toast.LENGTH_SHORT).show();
                        } else {
                            progressDialog.dismiss();
                        }

                    }
                });
        requestQueue.add(request);

        if(!swipeRefreshLayout.isRefreshing()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Fetching Content....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    private ArrayList<Sexploration> parseJSONResponse(JSONObject response) {

        sexplorationArrayList = new ArrayList<Sexploration>();
        if (response != null || response.length() > 0) {
            try {

                if (response.has("categories")) {
                    JSONArray jsonArray = response.getJSONArray("categories");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject sexplorationObject = jsonArray.getJSONObject(i);
                        int id = sexplorationObject.getInt("id");
                        String name = sexplorationObject.getString("name");
                        int parentId = sexplorationObject.getInt("parent_id");
                        int subCategory = sexplorationObject.getInt("subcategories");
                            Sexploration sexploration = new Sexploration(id, name, parentId, subCategory);
                            sexploration.toString();
                        Log.d("Sexplo:", "" + sexploration);

                        if (i != 0) {
                            helper.saveSexploration(id,name);
                            sexplorationArrayList.add(sexploration);
                        }


                    }
                }

            } catch (JSONException jsonException) {

            }
        }
        return sexplorationArrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(broadcastReceiver);
        Log.d("Newly Wed", "onBackPressed");
    }

    public int getDatafromDB(){
        sexplorationArrayList = helper.getSexploration();
        return sexplorationArrayList.size();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
