package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.VendorType;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.VendorTypeViewHolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorTypeAdapter extends RecyclerView.Adapter<VendorTypeViewHolder> {

    private ArrayList<VendorType> vendorTypeArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private boolean fromDB;
    private Bitmap imageBitmap;
    private File storageDir;
    private File file;

    public VendorTypeAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        storageDir = new File(Environment.getExternalStorageDirectory() + "/newlywed/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();
    }

    public void setVendorTypeArrayList(ArrayList<VendorType> vendorTypeArrayList, boolean fromdB) {
        this.fromDB = fromdB;
        if (vendorTypeArrayList != null) {
            this.vendorTypeArrayList = vendorTypeArrayList;
            notifyItemRangeChanged(0, vendorTypeArrayList.size());
        }
    }


    @Override
    public VendorTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_vendor_type, parent, false);
        VendorTypeViewHolder viewHolderProfession = new VendorTypeViewHolder(view);
        return viewHolderProfession;
    }

    @Override
    public void onBindViewHolder(final VendorTypeViewHolder holder, int position) {

        final VendorType vendorType = vendorTypeArrayList.get(position);
        holder.tvVendorTypeName.setText(vendorType.getVendorTypeName());
        String urlImage = vendorType.getVendorTypeImage();
        if(!fromDB) {
            if (urlImage != null) {
                imageLoader.get(URLS.vendorTypeImageUrl + urlImage, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.ivVendorTypeImage.setImageBitmap(response.getBitmap());

                        imageBitmap = response.getBitmap();

                        if(imageBitmap != null) {
                            File imageFile = new File(file, vendorType.getVendorTypeImage());
                            try {
                                FileOutputStream out = new FileOutputStream(imageFile);
                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " File not Found");
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " IO Exception");
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.ivVendorTypeImage.setImageResource(R.drawable.no_image);
                    }
                });
            }
        } else {
            File imgFile = new File(storageDir + "/" + vendorType.getVendorTypeImage());
            if(imgFile.exists()){
                Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                if(imgBitmap != null){
                    holder.ivVendorTypeImage.setImageBitmap(imgBitmap);
                } else {
                    holder.ivVendorTypeImage.setImageResource(R.drawable.no_image);
                }

            }
            else {
                holder.ivVendorTypeImage.setImageResource(R.drawable.no_image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return vendorTypeArrayList.size();
    }
}
