package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.VendorViewHolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/4/2015.
 */
public class VendorAdapter extends RecyclerView.Adapter<VendorViewHolder> {

    private ArrayList<Vendor> vendorArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private boolean fromDB;
    private Bitmap imageBitmap;
    private File storageDir;
    private File file;

    public VendorAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        storageDir = new File(Environment.getExternalStorageDirectory() + "/newlywed/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();
    }

    public void setVendorArrayList(ArrayList<Vendor> vendorArrayList,boolean fromDB) {
        this.fromDB = fromDB;
        if (vendorArrayList != null) {
            this.vendorArrayList = vendorArrayList;
            notifyItemRangeChanged(0, vendorArrayList.size());
        }
    }


    @Override
    public VendorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_vendor, parent, false);
        VendorViewHolder viewHolderProfession = new VendorViewHolder(view);
        return viewHolderProfession;
    }

    @Override
    public void onBindViewHolder(final VendorViewHolder holder, int position) {

        final Vendor vendor = vendorArrayList.get(position);
        holder.tvVendorName.setText(vendor.getVendorName());
        String urlImage = vendor.getVendorImage();
        if(!fromDB) {
            if (urlImage != null) {
                imageLoader.get(URLS.vendorImageUrl + urlImage, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.ivVendorImage.setImageBitmap(response.getBitmap());
                        imageBitmap = response.getBitmap();

                        if(imageBitmap != null) {
                            File imageFile = new File(file, vendor.getVendorImage());
                            try {
                                FileOutputStream out = new FileOutputStream(imageFile);
                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " File not Found");
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " IO Exception");
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.ivVendorImage.setImageResource(R.drawable.no_image);
                    }
                });
            }
        } else {
            File imgFile = new File(storageDir + "/" + vendor.getVendorImage());
            if(imgFile.exists()){
                Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                if(imgBitmap != null){
                    holder.ivVendorImage.setImageBitmap(imgBitmap);
                } else {
                    holder.ivVendorImage.setImageResource(R.drawable.no_image);
                }

            }
            else {
                holder.ivVendorImage.setImageResource(R.drawable.no_image);
            }

        }
    }

    @Override
    public int getItemCount() {
        return vendorArrayList.size();
    }
}
