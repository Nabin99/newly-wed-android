package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 8/5/2015.
 */
public class Product implements Serializable{
    private int id;
    private String productName;
    private String description;
    private int quantity;
    private String salePrice;
    private String imageUrl;

    public Product() {
    }

    public Product(int id, String productName, String description, int quantity, String salePrice, String imageUrl) {
        this.id = id;
        this.productName = productName;
        this.description = description;
        this.quantity = quantity;
        this.salePrice = salePrice;
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", salePrice='" + salePrice + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
