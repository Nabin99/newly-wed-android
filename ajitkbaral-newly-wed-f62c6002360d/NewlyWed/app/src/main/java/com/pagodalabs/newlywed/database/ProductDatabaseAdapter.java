package com.pagodalabs.newlywed.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pagodalabs.newlywed.entities.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class ProductDatabaseAdapter {
    private DatabaseHelper databaseHelper;
    private Context context;

    public ProductDatabaseAdapter(Context context) {
        databaseHelper = new DatabaseHelper(context);
        this.context = context;
    }

    public long insertProduct(Product product, int category) {
        long insert = 0;
        if (getByProductId(product.getId(), category) == null) {
            SQLiteDatabase sqliteDb = databaseHelper.getWritableDatabase();
            ContentValues values = productValues(product, category);
            insert = sqliteDb.insert(DatabaseHelper.TABLE_NAME_PRODUCTS, null, values);
        }
        return insert;
    }

    public int updateProductDetail(Product product, int category) {
        SQLiteDatabase sqliteDb = databaseHelper.getWritableDatabase();
        ContentValues values = productValues(product, category);
        return sqliteDb.update(DatabaseHelper.TABLE_NAME_PRODUCTS, values, DatabaseHelper.COLUMN_PRODUCT_ID + " = " + product.getId() + " AND " + DatabaseHelper.COLUMN_CART_OR_WISH_LIST + " = " + category, null);
    }

    public Product getByProductId(int productId, int category) {
        Product product = null;
        SQLiteDatabase sqliteDb = databaseHelper.getWritableDatabase();
        String[] columns = {DatabaseHelper.COLUMN_PRODUCT_ID,
                DatabaseHelper.COLUMN_PRODUCT_NAME,
                DatabaseHelper.COLUMN_DESCRIPTION,
                DatabaseHelper.COLUMN_QUANTITY,
                DatabaseHelper.COLUMN_SALE_PRICE,
                DatabaseHelper.COLUMN_IMAGE_URL};
        Cursor cursor = sqliteDb.query(DatabaseHelper.TABLE_NAME_PRODUCTS,
                columns,
                DatabaseHelper.COLUMN_PRODUCT_ID + " = " + productId + " AND " +
                DatabaseHelper.COLUMN_CART_OR_WISH_LIST + " = " + category,
                null, null, null, null);
        while (cursor.moveToNext()) {
            product = initProduct(cursor);
        }
        return product;
    }

    public int delete(Integer id, int category) {
        SQLiteDatabase sqliteDb = databaseHelper.getWritableDatabase();
        return sqliteDb.delete(DatabaseHelper.TABLE_NAME_PRODUCTS, DatabaseHelper.COLUMN_PRODUCT_ID + " = " + id + " AND " + DatabaseHelper.COLUMN_CART_OR_WISH_LIST + " = " + category, null);
    }


    public List<Product> getAll(int category) {
        List<Product> productList = new ArrayList<Product>();
        SQLiteDatabase sqliteDb = databaseHelper.getWritableDatabase();
        String[] columns = {DatabaseHelper.COLUMN_PRODUCT_ID, DatabaseHelper.COLUMN_PRODUCT_NAME, DatabaseHelper.COLUMN_DESCRIPTION,
                DatabaseHelper.COLUMN_QUANTITY, DatabaseHelper.COLUMN_SALE_PRICE, DatabaseHelper.COLUMN_IMAGE_URL};
        Cursor cursor = sqliteDb.query(DatabaseHelper.TABLE_NAME_PRODUCTS, columns, DatabaseHelper.COLUMN_CART_OR_WISH_LIST + " = " + category, null, null, null, null);
        while (cursor.moveToNext()) {
            Product product = initProduct(cursor);
            productList.add(product);
        }
        return productList;
    }


    private Product initProduct(Cursor cursor) {
        Product product = new Product();
        product.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PRODUCT_ID)));
        product.setProductName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PRODUCT_NAME)));
        product.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCRIPTION)));
        product.setQuantity(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_QUANTITY)));
        product.setSalePrice(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_SALE_PRICE)));
        product.setImageUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_IMAGE_URL)));
        return product;
    }

    private ContentValues productValues(Product product, int category) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_PRODUCT_ID, product.getId());
        values.put(DatabaseHelper.COLUMN_PRODUCT_NAME, product.getProductName());
        values.put(DatabaseHelper.COLUMN_DESCRIPTION, product.getDescription());
        values.put(DatabaseHelper.COLUMN_QUANTITY, product.getQuantity());
        values.put(DatabaseHelper.COLUMN_SALE_PRICE, product.getSalePrice());
        values.put(DatabaseHelper.COLUMN_IMAGE_URL, product.getImageUrl());
        values.put(DatabaseHelper.COLUMN_CART_OR_WISH_LIST, category);
        return values;
    }
}
