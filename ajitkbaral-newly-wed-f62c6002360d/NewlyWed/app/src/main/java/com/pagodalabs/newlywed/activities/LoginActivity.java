package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    private Context context;
    private EditText etUsernameOrEmail;
    private EditText etPassword;
    private Button btnLogin;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
        context = this;
        etUsernameOrEmail = (EditText) findViewById(R.id.etUsernameOrEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        volleySingleton = VolleySingleton.getInstance();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //btnLogin.setClickable(false);

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                String usernameOrEmail = etUsernameOrEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (usernameOrEmail.equals("") && password.equals("")) {
                    Toast.makeText(context, "Both fields are empty", Toast.LENGTH_SHORT).show();
                } else if (usernameOrEmail.equals("")) {
                    Toast.makeText(context, "Username/Email is empty", Toast.LENGTH_SHORT).show();
                } else if (password.equals("")) {
                    Toast.makeText(context, "Password is empty", Toast.LENGTH_SHORT).show();

                } else {
                    sendJsonRequest(usernameOrEmail, password);
                }

            }
        });

        btnLogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.buttonTouch(btnLogin, event);
                return false;
            }
        });

    }

    private void sendJsonRequest(String usernameOrEmail, String password) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", usernameOrEmail);
        params.put("password", password);
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URLS.loginPost,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response != null) {

                        Boolean success = false;
                        String accessToken = "";

                        if (response.has(Key.success)) {
                            success = (Boolean) response.get(Key.success);
                            if (success) {
                                Log.d("NewlyWed", "Success " + success);
                                try {
                                    int userId = 0;
                                    String username = "";

                                    JSONArray loginArray = response.getJSONArray(Key.userData);
                                    for (int i = 0; i < loginArray.length(); i++) {
                                        JSONObject loginObject = loginArray.getJSONObject(i);
                                        JSONObject obj = loginObject.getJSONObject("user");
                                        userId = obj.getInt("id");
                                        username = obj.getString("username");
                                        accessToken = loginObject.getString("access_token");

                                    }
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName,
                                            "userId", "" + userId);
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName,
                                            "username", username);
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName,
                                            "accessToken", accessToken);
                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName,
                                            "login", "loggedIn");

                                    long currentDateInEpoch = System.currentTimeMillis() / 1000;

                                    long sessionExpire = currentDateInEpoch + 86400;

                                    SharedPref.saveToPreferences(context, SharedPrefKey.fileName,
                                            "sessionExpire", "" + sessionExpire);


                                    Log.d("Newly Wed", "userId " + userId + " username " + username +
                                            " access token " + accessToken + " currentDateInEpoch " + currentDateInEpoch + " Expire Date " + sessionExpire);

                                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    btnLogin.setClickable(true);
                                    progressDialog.dismiss();
                                    finish();
                                    //((HomeActivity)context).finish();

                                } catch (JSONException jsonException) {

                                }
                            }else{
                                btnLogin.setClickable(true);
                                progressDialog.dismiss();
                                Toast.makeText(context, "Username or Password is incorrect",
                                        Toast.LENGTH_LONG).show();

                            }
                        }

                    }

                } catch (JSONException jsonException) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(context, VolleyErrorMessage.handleVolleyErrors(context, error),
                        Toast.LENGTH_LONG).show();
                btnLogin.setClickable(true);


            }
        });

        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Checking Credentials");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
