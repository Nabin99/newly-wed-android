package com.pagodalabs.newlywed.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "newlywed.db";
    public static final String COLUMN_KEY_ID = "_id";//used in all table
    private static final int DATABASE_VERSION = 2;

    //products
    public static final String TABLE_NAME_PRODUCTS = "products";
    public static final String COLUMN_PRODUCT_ID = "product_id";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_SALE_PRICE = "sale_price";
    public static final String COLUMN_IMAGE_URL = "image_url";
    public static final String COLUMN_CART_OR_WISH_LIST ="cart_or_wish_list";
    public static final String CREATE_DATABASE_TABLE_PRODUCTS = "CREATE TABLE "
            +TABLE_NAME_PRODUCTS+"( "+COLUMN_KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +COLUMN_PRODUCT_ID+" INTEGER,"+ COLUMN_PRODUCT_NAME+" VARCHAR(255), "+COLUMN_QUANTITY+" INTEGER, "
            +COLUMN_SALE_PRICE+" VARCHAR(255), "+COLUMN_DESCRIPTION+" TEXT, "+COLUMN_IMAGE_URL+" VARCHAR(255), "
            +COLUMN_CART_OR_WISH_LIST+" INTEGER);";
    private static final String DROP_DATABASE_TABLE_PRODUCTS = "DROP TABLE IF EXISTS "
            + TABLE_NAME_PRODUCTS;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(CREATE_DATABASE_TABLE_PRODUCTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL(DROP_DATABASE_TABLE_PRODUCTS);
        onCreate(db);
    }

}
