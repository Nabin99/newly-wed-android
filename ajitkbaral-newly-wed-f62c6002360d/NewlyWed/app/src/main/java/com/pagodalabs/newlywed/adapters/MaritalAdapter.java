package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Marital;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.MaritalViewHolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/6/2015.
 */
public class MaritalAdapter extends RecyclerView.Adapter<MaritalViewHolder> {

    private ArrayList<Marital> maritalArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private boolean fromDB;
    private Bitmap imageBitmap;
    private File storageDir;
    private File file;


    public MaritalAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        storageDir = new File(Environment.getExternalStorageDirectory() + "/newlywed/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();
    }

    public void setMaritalList(ArrayList<Marital> maritalArrayList, boolean fromDB){
        this.fromDB = fromDB;
        if (maritalArrayList != null) {
            this.maritalArrayList = maritalArrayList;
            notifyItemRangeChanged(0, maritalArrayList.size());
        }
    }

    @Override
    public MaritalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_marital, parent, false);
        MaritalViewHolder viewHolderMarital = new MaritalViewHolder(view);
        return viewHolderMarital;
    }

    @Override
    public void onBindViewHolder(final MaritalViewHolder holder, int position) {
        final Marital currentMarital = maritalArrayList.get(position);
        holder.tvPageTitle.setText(currentMarital.getPageTitle());
        String urlThumbnail = currentMarital.getImageName();

        if(!fromDB) {
            if (urlThumbnail != null) {

                imageLoader.get(URLS.maritalImageUrl + urlThumbnail, new ImageLoader.ImageListener() {

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.ivImageName.setImageBitmap(response.getBitmap());
                        imageBitmap = response.getBitmap();

                        if(imageBitmap != null) {
                            File imageFile = new File(file, currentMarital.getImageName());
                            try {
                                FileOutputStream out = new FileOutputStream(imageFile);
                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " File not Found");
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " IO Exception");
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.ivImageName.setImageResource(R.drawable.no_image);
                    }
                });
            }
        } else {
            File imgFile = new File(storageDir + "/" + currentMarital.getImageName());
            if(imgFile.exists()){
                Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                if(imgBitmap != null){
                    holder.ivImageName.setImageBitmap(imgBitmap);
                } else {
                    holder.ivImageName.setImageResource(R.drawable.no_image);
                }

            }
            else {
                holder.ivImageName.setImageResource(R.drawable.no_image);
            }
        }

    }

    @Override
    public int getItemCount() {
        return maritalArrayList.size();
    }
}
