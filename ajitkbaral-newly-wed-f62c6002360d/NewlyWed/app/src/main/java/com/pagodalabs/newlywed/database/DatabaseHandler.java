package com.pagodalabs.newlywed.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pagodalabs.newlywed.entities.Inspiration;
import com.pagodalabs.newlywed.entities.Marital;
import com.pagodalabs.newlywed.entities.Sexploration;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.entities.VendorType;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by apple on 6/14/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "newlywed";

    //Table Blog
    private static final String DATABASE_TABLE_BLOG = "blogs";
    private static final String KEY_PAGE_ID = "id";
    private static final String KEY_PAGE_TITLE = "title";
    private static final String KEY_PAGE_DESCRIPTION = "description";
    private static final String KEY_IMAGE_NAME = "image_name";
    private static final String KEY_MODIFIED_DATE = "modified_date";

    private static final String CREATE_TABLE_BLOG = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_BLOG + "(" + KEY_PAGE_ID + " INTEGER," +
            KEY_PAGE_TITLE + " VARCHAR," + KEY_PAGE_DESCRIPTION + " TEXT," + KEY_IMAGE_NAME + " TEXT," + KEY_MODIFIED_DATE + " TEXT )";

    //SExploration Table
    private static final String DATABASE_TABLE_SEXPLORATION = "sexploration";
    private static  final String KEY_SEXPLORATION_ID = "id";
    private static final String KEY_SEXPLORATION_NAME = "name";

    private static final String CREATE_TABLE_SEXPLORATION = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_SEXPLORATION + "(" + KEY_SEXPLORATION_ID + " INTEGER," +
             KEY_SEXPLORATION_NAME + " TEXT)";

    //SExploration Detail Table
    private static final String DATABASE_TABLE_SEXPLORATION_DETAILS = "sexploration_details";
    private static final String KEY_SEXPLORATION_PARENT_ID = "parent_id";
    private static final String KEY_SEXPLORATION_PAGE_ID = "page_id";
    private static final String KEY_SEXPLORATION_PAGE_TITLE = "page_title";
    private static final String KEY_SEXPLORATION_DESCRIPTION = "description";
    private static final String KEY_SEXPLORATION_IMAGE_NAME = "image_name";
    private static final String KEY_SEXPLORATION_MODIFIED_DATE = "modified_date";

    private static final String CREATE_TABLE_SEXPLORATION_DETAILS = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_SEXPLORATION_DETAILS + "(" + KEY_SEXPLORATION_PARENT_ID + " INTEGER," +
            KEY_SEXPLORATION_PAGE_ID + " INTEGER, " + KEY_SEXPLORATION_PAGE_TITLE + " TEXT, " + KEY_SEXPLORATION_DESCRIPTION + " TEXT, " + KEY_SEXPLORATION_IMAGE_NAME + " TEXT, "
            + KEY_SEXPLORATION_MODIFIED_DATE + " TEXT)";

    //VEndor Type Table
    private static final String DATABASE_TABLE_VENDOR_TYPE = "vendor_type";
    private static final String KEY_VENDOR_TYPE_ID  = "vendor_type_id";
    private static final String KEY_VENDOR_TYPE_NAME = "vendor_type_name";
    private static final String KEY_VENDOR_TYPE_IMAGE_NAME = "vendor_type_image_name";

    private static final String CREATE_TABLE_VENDOR_TYPE = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_VENDOR_TYPE + "(" + KEY_VENDOR_TYPE_ID + " INTEGER," +
            KEY_VENDOR_TYPE_NAME + " TEXT," + KEY_VENDOR_TYPE_IMAGE_NAME + " TEXT)";

    //VEndor Table
    private static final String DATABASE_TABLE_VENDORS = "vendors";
    private static final String KEY_VENDOR_ID  = "vendor_id";
    private static final String KEY_VENDOR_TYPENAME  = "vendor_type";
    private static final String KEY_VENDOR_TYPEID  = "vendor_type_id";
    private static final String KEY_VENDOR_NAME = "vendor_name";
    private static final String KEY_VENDOR_IMAGE_NAME = "vendor_image_name";
    private static final String KEY_VENDOR_DESCRIPTION = "vendor_description";
    private static final String KEY_VENDOR_ADDRESS = "vendor_address";
    private static final String KEY_VENDOR_PHONE = "vendor_phone";
    private static final String KEY_VENDOR_STATE = "vendor_state";
    private static final String KEY_VENDOR_CITY = "vendor_city";

    private static final String CREATE_TABLE_VENDORS = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_VENDORS + "(" + KEY_VENDOR_ID + " INTEGER," +
            KEY_VENDOR_TYPENAME + " TEXT," + KEY_VENDOR_NAME + " TEXT,"+ KEY_VENDOR_TYPEID + " TEXT," +  KEY_VENDOR_IMAGE_NAME + " TEXT," + KEY_VENDOR_DESCRIPTION + " TEXT," + KEY_VENDOR_ADDRESS + " TEXT," +
            KEY_VENDOR_PHONE + " TEXT," + KEY_VENDOR_STATE + " TEXT," + KEY_VENDOR_CITY + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("database123:", "" + CREATE_TABLE_VENDORS);
        db.execSQL(CREATE_TABLE_BLOG);
        db.execSQL(CREATE_TABLE_SEXPLORATION);
        db.execSQL(CREATE_TABLE_SEXPLORATION_DETAILS);
        db.execSQL(CREATE_TABLE_VENDOR_TYPE);
        db.execSQL(CREATE_TABLE_VENDORS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + DATABASE_TABLE_BLOG);
        db.execSQL("DROP TABLE " + DATABASE_TABLE_SEXPLORATION);
        db.execSQL("DROP TABLE " + DATABASE_TABLE_SEXPLORATION_DETAILS);
        db.execSQL("DROP TABLE " + DATABASE_TABLE_VENDOR_TYPE);
        db.execSQL("DROP TABLE " + DATABASE_TABLE_VENDORS);
        onCreate(db);

    }

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void saveBlog(Integer id,String page_title,String page_description,String image_name,String modified_date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PAGE_ID, id);
        values.put(KEY_PAGE_TITLE, page_title);
        values.put(KEY_PAGE_DESCRIPTION, page_description);
        values.put(KEY_IMAGE_NAME, image_name);
        values.put(KEY_MODIFIED_DATE, modified_date);
        db.insert(DATABASE_TABLE_BLOG, null, values);
    }

    public void clearBlog(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_BLOG);
    }

    public ArrayList<Inspiration> getBlogs(){

        ArrayList<Inspiration> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " + DATABASE_TABLE_BLOG, null);
        res.moveToFirst();

        while(!res.isAfterLast()){

            int id = res.getColumnIndex(KEY_PAGE_ID);
            int page_title = res.getColumnIndex(KEY_PAGE_TITLE);
            int page_description = res.getColumnIndex(KEY_PAGE_DESCRIPTION);
            int image_name = res.getColumnIndex(KEY_IMAGE_NAME);
            int modified_date = res.getColumnIndex(KEY_MODIFIED_DATE);

            Inspiration inspiration = new Inspiration();
            inspiration.setPageId(res.getInt(id));
            inspiration.setPageTitle(res.getString(page_title));
            inspiration.setDescription(res.getString(page_description));
            inspiration.setImageName(res.getString(image_name));
            inspiration.setModifiedDate(res.getString(modified_date));

            list.add(inspiration);
            res.moveToNext();
        }
        return list;
    }

    public void saveSexploration(Integer id, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SEXPLORATION_ID,id);
        values.put(KEY_SEXPLORATION_NAME, name);
        db.insert(DATABASE_TABLE_SEXPLORATION,null,values);
    }

    public void clearSexploration(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_SEXPLORATION);
    }

    public ArrayList<Sexploration> getSexploration(){
        ArrayList<Sexploration> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DATABASE_TABLE_SEXPLORATION, null);
        res.moveToFirst();

        while(!res.isAfterLast()){
            int id = res.getColumnIndex(KEY_SEXPLORATION_ID);
            int name = res.getColumnIndex(KEY_SEXPLORATION_NAME);

            Sexploration sexploration = new Sexploration();
            sexploration.setId(res.getInt(id));
            sexploration.setName(res.getString(name));
            list.add(sexploration);
            res.moveToNext();
        }
        return list;
    }

    public void saveSexplorationDetails(Integer parent_id,Integer page_id, String page_title, String page_description, String image_name , String modified_date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SEXPLORATION_PARENT_ID,parent_id);
        values.put(KEY_SEXPLORATION_PAGE_ID, page_id);
        values.put(KEY_SEXPLORATION_PAGE_TITLE,page_title);
        values.put(KEY_SEXPLORATION_DESCRIPTION, page_description);
        values.put(KEY_SEXPLORATION_IMAGE_NAME,image_name);
        values.put(KEY_SEXPLORATION_MODIFIED_DATE, modified_date);
        db.insert(DATABASE_TABLE_SEXPLORATION_DETAILS,null,values);
    }

    public void clearSexplorationDetails(Integer parent_id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_SEXPLORATION_DETAILS + " where " + KEY_SEXPLORATION_PARENT_ID + "=" + parent_id);
    }

    public ArrayList<Marital> getSexplorationDetails(Integer parent_id){
        ArrayList<Marital> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DATABASE_TABLE_SEXPLORATION_DETAILS + " where " + KEY_SEXPLORATION_PARENT_ID + "=" + parent_id, null);
        res.moveToFirst();

        while(!res.isAfterLast()){
            int parentId = res.getColumnIndex(KEY_SEXPLORATION_PARENT_ID);
            int pageId = res.getColumnIndex(KEY_SEXPLORATION_PAGE_ID);
            int title = res.getColumnIndex(KEY_SEXPLORATION_PAGE_TITLE);
            int description = res.getColumnIndex(KEY_SEXPLORATION_DESCRIPTION);
            int image_name = res.getColumnIndex(KEY_SEXPLORATION_IMAGE_NAME);
            int modified_date = res.getColumnIndex(KEY_SEXPLORATION_MODIFIED_DATE);

            Marital marital = new Marital();
            marital.setParent_id(res.getInt(parentId));
            marital.setPageId(res.getInt(pageId));
            marital.setPageTitle(res.getString(title));
            marital.setDescription(res.getString(description));
            marital.setImageName(res.getString(image_name));
            marital.setModifiedDate(res.getString(modified_date));
            list.add(marital);
            res.moveToNext();
        }
        return list;
    }

    public void saveVendorType(Integer id,String name, String image_name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VENDOR_TYPE_ID,id);
        values.put(KEY_VENDOR_TYPE_NAME, name);
        values.put(KEY_VENDOR_TYPE_IMAGE_NAME,image_name);
        db.insert(DATABASE_TABLE_VENDOR_TYPE,null,values);
    }

    public void clearVendorType(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_VENDOR_TYPE);
    }

    public ArrayList<VendorType> getVendorType(){
        ArrayList<VendorType> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DATABASE_TABLE_VENDOR_TYPE,null);
        res.moveToFirst();

        while(!res.isAfterLast()){
            int typeId = res.getColumnIndex(KEY_VENDOR_TYPE_ID);
            int name = res.getColumnIndex(KEY_VENDOR_TYPE_NAME);
            int image_name = res.getColumnIndex(KEY_VENDOR_TYPE_IMAGE_NAME);

            VendorType vendorType = new VendorType();
            vendorType.setVendorTypeId(res.getInt(typeId));
            vendorType.setVendorTypeName(res.getString(name));
            vendorType.setVendorTypeImage(res.getString(image_name));
            list.add(vendorType);
            res.moveToNext();
        }
        return list;
    }

    public void saveVendors(Integer vendor_id,String vendor_type_name, String vendor_name,String vendorTypeId,String vendor_image,String vendor_description,String vendor_address,String vendor_phone,String state,String city){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_VENDOR_ID,vendor_id);
        values.put(KEY_VENDOR_TYPENAME, vendor_type_name);
        values.put(KEY_VENDOR_NAME,vendor_name);
        values.put(KEY_VENDOR_TYPEID,vendorTypeId);
        values.put(KEY_VENDOR_IMAGE_NAME,vendor_image);
        values.put(KEY_VENDOR_DESCRIPTION, vendor_description);
        values.put(KEY_VENDOR_ADDRESS,vendor_address);
        values.put(KEY_VENDOR_PHONE,vendor_phone);
        values.put(KEY_VENDOR_STATE, state);
        values.put(KEY_VENDOR_CITY,city);
        db.insert(DATABASE_TABLE_VENDORS,null,values);
    }

    public void clearVendors(String vendorTypeId){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + DATABASE_TABLE_VENDORS + " where " + KEY_VENDOR_TYPEID + "=" + vendorTypeId);
    }

    public ArrayList<Vendor> getVendors(String vendorTypeId){
        ArrayList<Vendor> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        //String type = vendorTypeName.replace(" ", "%20");
        Cursor res = db.rawQuery("select * from " + DATABASE_TABLE_VENDORS + " where " + KEY_VENDOR_TYPEID + "= " + vendorTypeId,null);
        res.moveToFirst();

        while(!res.isAfterLast()){
            int vendor_id = res.getColumnIndex(KEY_VENDOR_ID);
            int vendor_type_name = res.getColumnIndex(KEY_VENDOR_TYPENAME);
            int vendor_name = res.getColumnIndex(KEY_VENDOR_NAME);
            int vendor_typeId = res.getColumnIndex(KEY_VENDOR_TYPEID);
            int vendor_image = res.getColumnIndex(KEY_VENDOR_IMAGE_NAME);
            int vendor_description = res.getColumnIndex(KEY_VENDOR_DESCRIPTION);
            int vendor_address = res.getColumnIndex(KEY_VENDOR_ADDRESS);
            int vendor_phone = res.getColumnIndex(KEY_VENDOR_PHONE);
            int state = res.getColumnIndex(KEY_VENDOR_STATE);
            int city = res.getColumnIndex(KEY_VENDOR_CITY);

            Vendor vendor = new Vendor();
            vendor.setVendorId(res.getInt(vendor_id));
            vendor.setVendorTypeName(res.getString(vendor_type_name));
            vendor.setVendorName(res.getString(vendor_name));
            vendor.setVendorTypeId(res.getString(vendor_typeId));
            vendor.setVendorImage(res.getString(vendor_image));
            vendor.setDescription(res.getString(vendor_description));
            vendor.setAddress(res.getString(vendor_address));
            vendor.setPhone(res.getString(vendor_phone));
            vendor.setState(res.getString(state));
            vendor.setCity(res.getString(city));
            list.add(vendor);
            res.moveToNext();
        }
        return list;
    }
}
