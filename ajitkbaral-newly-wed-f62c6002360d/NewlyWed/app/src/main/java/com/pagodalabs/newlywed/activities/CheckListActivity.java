package com.pagodalabs.newlywed.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.CheckListArrayAdapter;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.CheckList;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CheckListActivity extends AppCompatActivity {
    private Context context;
    private Toolbar toolbar;
    private ListView lvCheckList;
    private ArrayList<CheckList> checkLists;
    private List<CheckList> selectedList;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    private int userId;
    private String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        userId = Integer.parseInt(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "userId", ""));
        accessToken = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "accessToken", "");

        lvCheckList= (ListView)findViewById(R.id.lvCheckList);
        sendJsonRequest();

    }

    private void sendJsonRequest() {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonCheckList+userId+"&access_token="+accessToken+".json",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("NewlyWed", response.toString());
                        parseJSONResponse(response);
                        lvCheckList.setAdapter(new CheckListArrayAdapter(context, checkLists));
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d("errorCheck:","" + error.toString());

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private ArrayList<CheckList> parseJSONResponse(JSONObject response) {

        checkLists = new ArrayList<CheckList>();
        ArrayList<Integer> checked = new ArrayList<Integer>();
        Log.d("responsechecklist:","" + response);
        if (response != null || response.length() > 0) {
            try {

                if (response.has("user_checklist")) {
                    JSONArray arrayCheckList = response.getJSONArray("user_checklist");

                    for (int i = 0; i < arrayCheckList.length(); i++) {
                        JSONObject checkListJson = arrayCheckList.getJSONObject(i);
                        checked.add(checkListJson.getInt("checklist_id"));
                    }
                }

                if (response.has("master_checklist")) {
                    JSONArray arrayCheckList = response.getJSONArray("master_checklist");

                    for (int i = 0; i < arrayCheckList.length(); i++) {
                        JSONObject checkListJson = arrayCheckList.getJSONObject(i);
                        CheckList checkList = new CheckList();
                        checkList.setId((checkListJson.getInt("id")));
                        checkList.setTitle(checkListJson.getString("title"));
                        checkList.setParentName(checkListJson.getString("parent_name"));
                        checkList.setVendorType(checkListJson.getString("vendor_type"));
                        if(checked.contains(checkList.getId())&&!checkList.getParentName().equals("null")) {
                            checkList.setSelected(true);
                        }else{
                            checkList.setSelected(false);
                        }
                        checkLists.add(checkList);

                    }
                }



            } catch (JSONException jsonException) {

            }
        }
        return checkLists;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_check_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        selectedList = new ArrayList<CheckList>();
        //noinspection SimplifiableIfStatement
        if(id == R.id.action_save){
            for(int i=0; i<checkLists.size(); i++){
                CheckList checkList = checkLists.get(i);
                if(checkList.getSelected()==true)
                {
                    selectedList.add(checkList);
                }
            }

            int[] selected = new int[selectedList.size()];
            for(int i = 0; i<selectedList.size(); i++)
            {
                selected[i] = selectedList.get(i).getId();
            }

            save(selected);
        }

        return super.onOptionsItemSelected(item);
    }


    private void save(final int[] selected) {
        Log.d("Checklist Array String", Arrays.toString(selected));
        Log.d("Checklist Array","" + selected);
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("userId", ""+userId);
        params.put("access_token", accessToken);
        params.put("user_checklists", selected);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                URLS.saveCheckList, new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("NewlyWed", response.toString());
                        try {
                            if((Boolean)response.get("success")==true){
                                Toast.makeText(context, "Checklist Saved", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(context, CheckListActivity.class));
                                finish();
                            }else{
                                Toast.makeText(context, "Error in Saving", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Server Connection Error", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                });
        requestQueue.add(request);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Saving....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

}
