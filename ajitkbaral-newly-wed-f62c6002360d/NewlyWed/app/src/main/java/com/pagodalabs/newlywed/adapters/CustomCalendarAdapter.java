package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Appointment;

import java.util.List;

/**
 * Created by Ajit Kumar Baral on 8/28/2015.
 */
public class CustomCalendarAdapter extends BaseAdapter {

    private Context context;
    private List<Appointment> mutableAppointments;

    public CustomCalendarAdapter(Context context, List<Appointment> mutableAppointments){
        this.context = context;
        this.mutableAppointments = mutableAppointments;
    }

    @Override
    public int getCount() {
        return mutableAppointments.size();
    }

    @Override
    public Appointment getItem(int position) {
        return mutableAppointments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.layout_calendar_appointment, parent, false);

            //TextView tvAppointmentTime = (TextView)convertView.findViewById(R.id.tvAppointmentTime);
            TextView tvVendorName = (TextView)convertView.findViewById(R.id.tvVendorName);
            TextView tvAppointmentTitle = (TextView)convertView.findViewById(R.id.tvAppointmentTitle);
            TextView tvAppointmentDescription = (TextView)convertView.findViewById(R.id.tvAppointmentDescription);

            Appointment appointment = getItem(position);
            tvVendorName.setText(appointment.getVendorName());/*
            int date = appointment.getFrom().getDate();
            int month = appointment.getFrom().getMonth()+1;
            int year = appointment.getFrom().getYear();
            tvAppointmentTime.setText(""+date+"/"+month+"/"+year);*/
            tvAppointmentTitle.setText(appointment.getTitle());
            tvAppointmentDescription.setText(appointment.getDescription());

        }
        return convertView;
    }
}
