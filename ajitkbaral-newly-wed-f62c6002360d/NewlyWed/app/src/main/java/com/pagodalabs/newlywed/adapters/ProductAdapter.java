package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.entities.Product;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.Utils;
import com.pagodalabs.newlywed.viewholder.ProductViewHolder;

import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/5/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private ArrayList<Product> productArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    public ProductAdapter(Context context){
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
    }

    public void setProductList(ArrayList<Product> productArrayList){
        if (productArrayList != null) {
            this.productArrayList = productArrayList;
            notifyItemRangeChanged(0, productArrayList.size());
        }
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_product, parent, false);
        ProductViewHolder viewHolderProduct = new ProductViewHolder(view);
        return viewHolderProduct;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Product currentProduct = productArrayList.get(position);
        holder.tvProductName.setText(currentProduct.getProductName());

        holder.tvProductSalePrice.setText("Price: NRS. "+ Utils.priceInFormat(currentProduct.getSalePrice()));
        String urlThumbnail = currentProduct.getImageUrl();

        if(urlThumbnail!=null){

            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.ivProductImage.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivProductImage.setImageResource(R.drawable.no_image);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }
}
