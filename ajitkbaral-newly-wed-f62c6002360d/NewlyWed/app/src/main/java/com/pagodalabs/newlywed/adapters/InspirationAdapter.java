package com.pagodalabs.newlywed.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.entities.Inspiration;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.viewholder.InspirationViewHolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ajit Kumar Baral on 8/3/2015.
 */
public class InspirationAdapter extends RecyclerView.Adapter<InspirationViewHolder>{

    private ArrayList<Inspiration> inspirationArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private boolean fromDB;
    private File storageDir;
    private File file;
    private Bitmap imageBitmap;


    public InspirationAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        storageDir = new File(Environment.getExternalStorageDirectory() + "/newlywed/");
        file = new File(String.valueOf(storageDir));
        file.mkdirs();

    }

    public void setInspirationArrayList(ArrayList<Inspiration> inspirationArrayList,Boolean fromDB) {
        this.fromDB = fromDB;
        if (inspirationArrayList != null) {
            this.inspirationArrayList = inspirationArrayList;
            notifyItemRangeChanged(0, inspirationArrayList.size());
        }
    }

    @Override
    public InspirationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_inspiration, parent, false);
        InspirationViewHolder inspirationViewHolder = new InspirationViewHolder(view);
        return inspirationViewHolder;
    }

    @Override
    public void onBindViewHolder(final InspirationViewHolder holder, int position) {
        final Inspiration currentInspiration = inspirationArrayList.get(position);
        holder.tvPageTitle.setText(currentInspiration.getPageTitle());
        String urlThumbnail = URLS.inspirationImageUrl + currentInspiration.getImageName();
        Log.d("Thumbnail","" + urlThumbnail);

        if(!fromDB) {
            if (urlThumbnail != null) {

                imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.ivImageName.setImageBitmap(response.getBitmap());
                        imageBitmap = response.getBitmap();

                        if(imageBitmap != null) {
                            File imageFile = new File(file, currentInspiration.getImageName());
                            Log.d("FileName1:", "" + imageFile);
                            try {
                                FileOutputStream out = new FileOutputStream(imageFile);
                                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                                out.flush();
                                out.close();
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " File not Found");
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.d("Saving error:", " IO Exception");
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.ivImageName.setImageResource(R.drawable.no_image);
                    }
                });
            }
        } else {
            File imgFile = new File(storageDir + "/" + currentInspiration.getImageName());
            Log.d("FileName1:", "" + imgFile);
            if(imgFile.exists()){
                Log.d("FileName:", "" + imgFile);
                Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                if(imgBitmap != null){
                    holder.ivImageName.setImageBitmap(imgBitmap);
                } else {
                    holder.ivImageName.setImageResource(R.drawable.no_image);
                }

            }
            else {
                holder.ivImageName.setImageResource(R.drawable.no_image);
            }

        }

    }

    @Override
    public int getItemCount() {
        return inspirationArrayList.size();
    }

}

