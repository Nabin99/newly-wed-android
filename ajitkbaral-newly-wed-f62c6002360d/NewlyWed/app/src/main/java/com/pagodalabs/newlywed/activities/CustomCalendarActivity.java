package com.pagodalabs.newlywed.activities;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.CalendarDayEvent;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.adapters.CustomCalendarAdapter;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.constant.URLS;
import com.pagodalabs.newlywed.dialogs.AppointmentDialog;
import com.pagodalabs.newlywed.entities.Appointment;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;
import com.pagodalabs.newlywed.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class CustomCalendarActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Context context;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private Map<Date, List<Appointment>> appointments = new HashMap<>();
    private TextView tvMonthName;
    private TextView tvEmptyList;

    private ListView appointmentsListView;
    private Button showPreviousMonthBut;
    private Button showNextMonthBut;

    private ArrayAdapter adapter;
    private CompactCalendarView compactCalendarView;

    private List<Appointment> mutableAppointments;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;
    private int userId;
    private String accessToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_calendar);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();

        userId = Integer.parseInt(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "userId", ""));
        accessToken = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "accessToken", "");
        mutableAppointments = new ArrayList<>();

        appointmentsListView = (ListView) findViewById(R.id.appointmentsListView);
        showPreviousMonthBut = (Button) findViewById(R.id.prev_button);
        showNextMonthBut = (Button) findViewById(R.id.next_button);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mutableAppointments);
        appointmentsListView.setAdapter(new CustomCalendarAdapter(context, mutableAppointments));
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendarView.drawSmallIndicatorForEvents(true);
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        addEvents(compactCalendarView);


        //set initial month name
        tvMonthName = (TextView) findViewById(R.id.tvMonthName);
        tvMonthName.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        tvEmptyList = (TextView) findViewById(R.id.tvEmptyList);



        //set title on calendar scroll
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                displayList(dateClicked);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                tvMonthName.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
                displayList(firstDayOfNewMonth);

            }
        });

        showPreviousMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showPreviousMonth();
            }
        });

        showNextMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showNextMonth();
            }
        });


    }

    private void addEvents(final CompactCalendarView compactCalendarView) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URLS.jsonCalendar+userId+"&access_token="+accessToken+".json", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Newly Wed", response.toString());
                if (response.has("success")) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("calendar");
                        List<Appointment> appointmentList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String title = jsonObject.getString("title");
                            String description = jsonObject.getString("description");
                            int guests = jsonObject.getInt("guests");
                            String venueName = jsonObject.getString("venue_name");
                            String vendorName = jsonObject.getString("vendor_name");
                            String from = jsonObject.getString("from");
                            String to = jsonObject.getString("to");
                            String status = jsonObject.getString("status");

                            DateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd");
                            Date dateFrom = formatFrom.parse(from);
                            DateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");
                            Date dateTo = formatFrom.parse(to);

                            Appointment appointment = new Appointment(id, title, description, guests,
                                    venueName, vendorName, dateFrom, dateTo, status);
                            Log.d("Newly Wed", appointment.toString());
                            appointmentList.add(appointment);

                            for(int j = 0; j<appointmentList.size(); j++){
                                notifyCalendar(appointmentList.get(j).getFrom().getTime());
                            }
                            setToMidnight(currentCalender);
                            appointments.put(dateFrom, appointmentList);


                        }

                        progressDialog.dismiss();
                        displayList(currentCalender.getTime());
                    } catch (JSONException jsonException) {

                        progressDialog.dismiss();

                    } catch (ParseException parseException) {

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, VolleyErrorMessage.handleVolleyErrors(context, error), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }
        });

        requestQueue.add(jsonObjectRequest);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Content....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        /*for (int i = 0; i < 6; i++){
        currentCalender.setTimeInMillis(System.currentTimeMillis());
        currentCalender.add(Calendar.DATE, i);
        compactCalendarView.addEvent(new CalendarDayEvent(currentCalender.getTimeInMillis(), Color.argb(255, 169, 68, 65)));
        setToMidnight(currentCalender);
        appointments.put(currentCalender.getTime(), createAppointments());
        }*/

    }



    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public void displayList(Date dateClicked) {

        List<Appointment> appointmentsFromMap = appointments.get(dateClicked);
        Log.d("CustomCalendarActivity", "inside onclick " + dateClicked.getDate() + " "
                + dateClicked.getMonth() + " " + dateClicked.getTime());
        if (appointmentsFromMap != null) {
            tvEmptyList.setVisibility(View.GONE);
            appointmentsListView.setVisibility(View.VISIBLE);
            Log.d("CustomCalendarActivity", appointmentsFromMap.toString());
            mutableAppointments.clear();
            for (int i=0;i<appointmentsFromMap.size();i++){
                if(appointmentsFromMap.get(i).getFrom().getTime() == dateClicked.getTime()){
                    Appointment appointment = appointmentsFromMap.get(i);
                    mutableAppointments.add(appointment);
                }
            }

            appointmentsListView.setAdapter(new CustomCalendarAdapter(context, mutableAppointments));
            adapter.notifyDataSetChanged();
        } else {
            tvEmptyList.setVisibility(View.VISIBLE);
            appointmentsListView.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_custom_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_addAppointments:

                FragmentManager manager = getFragmentManager();
                AppointmentDialog appointmentDialog = new AppointmentDialog();
                appointmentDialog.setVendor(true);
                appointmentDialog.setContext(context);
                appointmentDialog.show(manager, "Add Appointment");
                break;

            case android.R.id.home:
                Intent homeIntent = new Intent(this, HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                break;

        }


        return super.onOptionsItemSelected(item);
    }

    private void notifyCalendar(long dateToMilliSecond){

        compactCalendarView.addEvent(new CalendarDayEvent(dateToMilliSecond,
                Color.argb(255, 169, 68, 65)));
    }
}