package com.pagodalabs.newlywed.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.broadcast.KillActivity;
import com.pagodalabs.newlywed.constant.Key;
import com.pagodalabs.newlywed.database.ProductDatabaseAdapter;
import com.pagodalabs.newlywed.entities.Product;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.Utils;

public class ProductDetailActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ImageView ivProductImage;
    private TextView tvProductName;
    private TextView tvProductSalePrice;
    private WebView tvProductDescription;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView tvQuantity;
    private TextView tvQuantityChange;
    private Button btnMinus;
    private Button btnPlus;
    private Button btnCart;
    private Button btnWishList;
    private int quantityChange = 1;
    private int cartFlag;
    private int wishListFlag;
    private ProductDatabaseAdapter productDatabaseAdapter;
    private Product product;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        context = this;
        productDatabaseAdapter = new ProductDatabaseAdapter(context);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvProductName = (TextView) findViewById(R.id.tvProductName);
        ivProductImage = (ImageView) findViewById(R.id.ivProductImage);
        tvProductDescription = (WebView) findViewById(R.id.tvProductDescription);
        tvProductSalePrice = (TextView) findViewById(R.id.tvProductSalePrice);
        tvQuantity = (TextView) findViewById(R.id.tvQuantity);
        tvQuantityChange = (TextView) findViewById(R.id.tvQuantityChange);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnCart = (Button) findViewById(R.id.btnCart);
        btnWishList = (Button) findViewById(R.id.btnWishList);
        tvQuantityChange.setText("" + quantityChange);


        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        requestQueue = volleySingleton.getRequestQueue();

        product = (Product) getIntent().getSerializableExtra("product");


        if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryCart) != null) {
            cartFlag = 1;
            btnCart.setText("Remove From Cart");
        }
        if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryWishList) != null) {
            wishListFlag = 1;
            btnWishList.setText("Remove from wish list");
        }

        tvProductName.setText(product.getProductName());

        String urlThumbnail = product.getImageUrl();

        if (urlThumbnail != null) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                @Override
                public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {
                    ivProductImage.setImageBitmap(response.getBitmap());
                    /*ivProductImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                            nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            nagDialog.setCancelable(false);
                            nagDialog.setContentView(R.layout.layout_preview_image);
                            Button btnClose = (Button) nagDialog.findViewById(R.id.btnIvClose);
                            ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);
                            ivPreview.setImageBitmap(response.getBitmap());

                            btnClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {

                                    nagDialog.dismiss();
                                }
                            });
                            nagDialog.show();
                        }
                    });*/
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    ivProductImage.setImageResource(R.drawable.no_image);
                    Toast.makeText(context, "Unable to load image", Toast.LENGTH_LONG).show();
                }
            });
        }

        tvQuantity.setText("Total Quantity: " + product.getQuantity());
        tvProductSalePrice.setText("NRS. " + Utils.priceInFormat(product.getSalePrice()));

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantityChange > 1) {

                    quantityChange = quantityChange - 1;
                }
                tvQuantityChange.setText("" + quantityChange);
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantityChange < product.getQuantity()) {
                    quantityChange = quantityChange + 1;
                }
                tvQuantityChange.setText("" + quantityChange);
            }
        });

        btnMinus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.buttonTouch(btnMinus, event);
                return false;
            }
        });

        btnPlus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.buttonTouch(btnPlus, event);
                return false;
            }
        });

        btnCart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.buttonTouch(btnCart, event);
                return false;
            }
        });

        btnWishList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Utils.buttonTouch(btnWishList, event);
                return false;
            }
        });

        //tvProductDescription.setText(product.getDescription().replace("<p>", "").replace("</p>", ". \n").replace("&rsquo;", "'"));
        tvProductDescription.loadData(product.getDescription(),"text/html; charset=utf-8", "UTF-8");

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryCart) == null) {
                    alertDialog.setTitle("Adding to cart");
                    alertDialog.setMessage("Do you want to add " + product.getProductName() + " to cart?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            double price = Double.parseDouble(product.getSalePrice());
                            product.setQuantity(quantityChange);
                            product.setSalePrice("" + quantityChange * price);
                            long insert = productDatabaseAdapter.insertProduct(product, Key.categoryCart);

                            if (insert >= 1) {
                                btnCart.setText("Remove from cart");
                                Toast.makeText(context, "Added to cart", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, "Already available in the cart", Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                    alertDialog.setNegativeButton("No", null);
                    alertDialog.show();
                } else {
                    alertDialog.setTitle("Remove from cart");
                    alertDialog.setMessage("Do you want to remove " + product.getProductName() + " from cart?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int delete = productDatabaseAdapter.delete(product.getId(), Key.categoryCart);
                            if (delete >= 1) {
                                Toast.makeText(context, "Removed from cart", Toast.LENGTH_LONG).show();
                                btnCart.setText("Add to Cart");
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", null);
                    alertDialog.show();
                }
            }


        });

        btnWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryWishList) == null) {
                    alertDialog.setTitle("Adding to wish list");
                    alertDialog.setMessage("Do you want to add " + product.getProductName() + " to wish list?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            long insert = productDatabaseAdapter.insertProduct(product, Key.categoryWishList);

                            if (insert >= 1) {
                                btnWishList.setText("Remove from wish list");
                                Toast.makeText(context, "Added to wish list", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(context, "Unable to add to wish list", Toast.LENGTH_LONG).show();
                            }

                        /*HashMap<String, String> params = new HashMap<String, String>();
                        params.put("userId", SharedPref.readFromPreferences(context, SharedPrefKey.fileName,
                                "19", ""));
                        params.put("productId", "" + product.getId());
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URLS.wishlistPost,
                                new JSONObject(params), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Newly Wed", response.toString());

                                if (response.has(Key.success)) {
                                    try {
                                        Boolean success = response.getBoolean(Key.success);
                                        if (success) {
                                            Toast.makeText(context, "Added to the wish list", Toast.LENGTH_LONG).show();
                                        } else {

                                            Toast.makeText(context, "Unable to add to wish list", Toast.LENGTH_LONG).show();
                                        }

                                    } catch (JSONException jsonException) {

                                    }
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, VolleyErrorMessage.handleVolleyErrors(context, error), Toast.LENGTH_LONG).show();

                            }
                        });
                        requestQueue.add(jsonObjectRequest);*/
                        }
                    });
                    alertDialog.setNegativeButton("No", null);
                    alertDialog.show();

                } else {
                    alertDialog.setTitle("Remove from wish list");
                    alertDialog.setMessage("Do you want to remove " + product.getProductName() + " from wish list?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int delete = productDatabaseAdapter.delete(product.getId(), Key.categoryWishList);
                            if (delete >= 1) {
                                Toast.makeText(context, "Removed from wish list", Toast.LENGTH_LONG).show();
                                btnWishList.setText("Add to wish list");
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", null);
                    alertDialog.show();
                }
            }
        });


        broadcastReceiver = KillActivity.killActivity(this);
        KillActivity.registerReceiver(context);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            Log.d("Newly Wed", "Up Pressed");
            unregisterReceiver(broadcastReceiver);
        }

        //noinspection SimplifiableIfStatement
        Utils.popUpMenu(context, id);

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryCart) == null) {
                btnCart.setText("Add to Cart");
                cartFlag = 0;
            }

        }
        if(requestCode == 2){
        if (productDatabaseAdapter.getByProductId(product.getId(), Key.categoryWishList) == null) {
            btnWishList.setText("Add to wish list");
            wishListFlag = 0;
        }
        }
    }




}
