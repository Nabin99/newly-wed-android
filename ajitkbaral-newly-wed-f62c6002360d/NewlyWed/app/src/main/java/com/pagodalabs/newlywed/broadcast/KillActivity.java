package com.pagodalabs.newlywed.broadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by Ajit Kumar Baral on 8/8/2015.
 */
public class KillActivity {



    /*public static void killActivities(Context context, final Activity activity){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.newlywed.ACTION_LOGOUT");
        LocalBroadcastManager.getInstance(context).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point we should start the login activity and finish this one.
                activity.finish();
            }
        }, intentFilter);
    }*/





    private static BroadcastReceiver broadcastReceiver;
    private static IntentFilter intentFilter;

    public static BroadcastReceiver killActivity(final Activity activity){
        intentFilter = new IntentFilter();
        intentFilter.addAction("CLOSE_ALL");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // close activity
                activity.finish();
            }
        };
        return broadcastReceiver;

    }

    public static void registerReceiver(Context context){
        context.registerReceiver(broadcastReceiver, intentFilter);
    }


}
