package com.pagodalabs.newlywed.entities;

import java.io.Serializable;

/**
 * Created by Ajit Kumar Baral on 1/5/2016.
 */
public class CheckList implements Serializable {
    private int id;
    private String title;
    private int parentId;
    private String vendorType;
    private String parentName;
    private boolean selected;

    public CheckList() {
    }

    public CheckList(int id, String title, int parentId, String vendorType, String parentName) {
        this.id = id;
        this.title = title;
        this.parentId = parentId;
        this.vendorType = vendorType;
        this.parentName = parentName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "CheckList{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", parentId=" + parentId +
                ", vendorType='" + vendorType + '\'' +
                ", parentName='" + parentName + '\'' +
                '}';
    }
}
