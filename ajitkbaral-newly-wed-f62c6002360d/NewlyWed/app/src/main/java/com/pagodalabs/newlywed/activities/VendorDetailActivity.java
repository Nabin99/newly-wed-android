package com.pagodalabs.newlywed.activities;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.pagodalabs.newlywed.R;
import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.dialogs.AppointmentDialog;
import com.pagodalabs.newlywed.entities.Vendor;
import com.pagodalabs.newlywed.singleton.VolleySingleton;
import com.pagodalabs.newlywed.utils.SharedPref;

import java.io.File;

public class VendorDetailActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;
    private RequestQueue requestQueue;
    private TextView tvVendorName;
    private WebView tvDescription;
    private TextView tvAddress;
    private ImageView ivVendorImage;
    private Button btnAppointment;
    private TextView tvPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_detail);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        final Vendor vendor = (Vendor)getIntent().getSerializableExtra("vendor");
        Boolean fromDB = getIntent().getBooleanExtra("fromDB",false);
        tvVendorName = (TextView)findViewById(R.id.tvVendorName);
        tvAddress = (TextView)findViewById(R.id.tvAddress);
        tvDescription = (WebView)findViewById(R.id.tvDescription);
        ivVendorImage = (ImageView)findViewById(R.id.ivVendorImage);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        btnAppointment = (Button)findViewById(R.id.btnAppointment);
        if(SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "").equalsIgnoreCase("")){
            btnAppointment.setVisibility(View.GONE);
        }else{
            btnAppointment.setVisibility(View.VISIBLE);
        }
        btnAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                AppointmentDialog appointmentDialog = new AppointmentDialog();
                appointmentDialog.setVendor(false);
                appointmentDialog.setContext(context);
                appointmentDialog.setVendorId(vendor.getVendorId());
                appointmentDialog.show(manager, "Add Appointment");
            }
        });
        tvVendorName.setText(vendor.getVendorName());
        tvAddress.setText(vendor.getAddress());
        tvPhone.setText(vendor.getPhone());
        Log.d("Vendor:", "" + vendor.getDescription());
        //tvDescription.setText(vendor.getDescription());
        tvDescription.loadData(vendor.getDescription(), "text/html; charset=utf-8", "UTF-8");
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();
        requestQueue = volleySingleton.getRequestQueue();

        String urlThumbnail = vendor.getVendorImage();

        if (urlThumbnail != null) {
            if (!fromDB) {
                imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {

                    @Override
                    public void onResponse(final ImageLoader.ImageContainer response, boolean isImmediate) {
                        ivVendorImage.setImageBitmap(response.getBitmap());
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ivVendorImage.setImageResource(R.drawable.no_image);
                    }
                });
            } else {
                File imgFile = new File(Environment.getExternalStorageDirectory() + "/newlywed" + "/" + vendor.getVendorImage());

                if(imgFile.exists()){
                    Log.d("FileName:", "" + imgFile);
                    Bitmap imgBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    if(imgBitmap != null){
                        ivVendorImage.setImageBitmap(imgBitmap);
                    } else {
                        ivVendorImage.setImageResource(R.drawable.no_image);
                    }

                }
                else {
                    ivVendorImage.setImageResource(R.drawable.no_image);
                }
            }
        }
    }

}
