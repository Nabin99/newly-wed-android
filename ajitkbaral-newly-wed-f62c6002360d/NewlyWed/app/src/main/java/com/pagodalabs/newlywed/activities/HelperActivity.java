package com.pagodalabs.newlywed.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pagodalabs.newlywed.constant.SharedPrefKey;
import com.pagodalabs.newlywed.utils.SharedPref;

public class HelperActivity extends AppCompatActivity {

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        String userId = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "userId", "");
        String username = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "username", "");
        String accessToken = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "accessToken", "");
        String login =SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "login", "");


        String expire = SharedPref.readFromPreferences(context, SharedPrefKey.fileName, "sessionExpire", "");

        long currentDateInEpoch = System.currentTimeMillis()/1000;

        long sessionExpire = 0;
        if(!expire.equals("")){
            sessionExpire = Long.parseLong(expire);
        }

        /*if (!userId.equals("")
                || !username.equals("")
                || !accessToken.equals("")
                || sessionExpire != 0) {
            if(currentDateInEpoch<sessionExpire) {
                startActivity(new Intent(context, HomeActivity.class));
            }else{
                startActivity(new Intent(context, LoginActivity.class));
            }

        }else{
            startActivity(new Intent(context, LoginActivity.class));
        }*/

        startActivity(new Intent(context, HomeActivity.class));
        finish();

    }
}
